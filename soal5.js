const nilai = [1, 7, 3, 4];
const n = nilai.length;

function hasilPerkalian(nilai) {
    const hasil = [];
    for (let i = 0; i < n; i++) {
        let nSekarang = 1;
        for (let j = 0; j < n; j++) {
            if (i !== j) {
                nSekarang *= nilai[j];
            }
        }
        hasil.push(nSekarang);
    }
    return hasil;
}

console.log(hasilPerkalian(nilai));
