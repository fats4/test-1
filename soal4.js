function simpleFunction(n) {
    for (let i = 1; i <= n; i++) {
        let j = 1;
        let output = "";
        while (j <= n + 3) {
            output += j;
            if (i === j) {
                output += "**";
                j += 2;
            }
            j++;
        }
        console.log(output);
    }
}

var n = 5;
console.log("input n = ", n)
simpleFunction(n);

console.log("\n");

var n = 4;
console.log("input n = ", n)
simpleFunction(n);
