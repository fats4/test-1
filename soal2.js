function kataUnik(kata) {
    const hurufUnik = {};
    for (let huruf of kata) {
        if (hurufUnik[huruf]) {
            hurufUnik[huruf]++;
        } else {
            hurufUnik[huruf] = 1;
        }
    }

    let hasil = "";
    for (let kata in hurufUnik) {
        hasil += `${kata}:${hurufUnik[kata]} `;
    }
    return hasil.trim();
}

const word = "racecarr!";
const hurufUnik = kataUnik(word);
console.log(`Kata input "${word}" substring : "${hurufUnik}".`);