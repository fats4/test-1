function isPalindrome(str) {
	for(let i = 0; i < str.length; i++){
        console.log(str[i]);                    // str[i] berfungsi untuk menampilkan string dari variabel i
        console.log(str[str.length - i -1]);    // menampilkan jumlah item array dari string dikurangi variabel i dan dikurangi 1
        if(str[i] != str[str.length - i -1]){   // menggunakan loop, jika str dari variabel i tidak sama dengan panjang jumlah item array
            return false;
        }
    }
    return true;
}

console.log(isPalindrome("aba"));
console.log(isPalindrome("racecar"));
console.log(isPalindrome("abc"));

// output true, true, false
